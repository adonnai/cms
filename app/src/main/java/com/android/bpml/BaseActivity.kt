package com.android.bpml

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.android.bpml.dagger2.components.ActivityComponent
import com.android.bpml.dagger2.components.DaggerActivityComponent
import com.android.bpml.dagger2.modules.ActivityModule

abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun getActivityComponent() : ActivityComponent {
        return DaggerActivityComponent.builder()
                .apply {
                    activityModule(ActivityModule(this@BaseActivity))
                    appComponent((INSTANCE as CMSApp).appComponent)
                    cmsAppComponent((INSTANCE as CMSApp).cmsAppComponent)
                }
                .build()
    }
}