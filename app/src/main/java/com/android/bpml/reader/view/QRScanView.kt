package com.android.bpml.reader.view

import android.view.Menu
import com.android.bpml.common.view.BaseView
import me.dm7.barcodescanner.zxing.ZXingScannerView

interface QRScanView : BaseView {
    fun initViews(handler : ZXingScannerView.ResultHandler)

    fun handleOnResume()

    fun handleOnPause()

    fun handleOnCreateOptionsMenu(menu : Menu?) : Boolean

    fun updateView(result : String?)

    fun resumeCameraPreview(handler : ZXingScannerView.ResultHandler)
}