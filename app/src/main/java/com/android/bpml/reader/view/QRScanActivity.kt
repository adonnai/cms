package com.android.bpml.reader.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import com.android.bpml.BaseActivity
import com.android.bpml.INSTANCE
import com.android.bpml.R
import com.android.bpml.reader.presenter.QRScanPresenter
import com.android.bpml.reader.router.QRScanRouter
import com.android.bpml.utils.CAMERA_REQUEST_CODE
import javax.inject.Inject

class QRScanActivity : BaseActivity() {
    @Inject lateinit var qrScanView : QRScanView
    @Inject lateinit var qrScanPresenter : QRScanPresenter
    @Inject lateinit var qrScanRouter : QRScanRouter

    companion object {
        fun startActivity() {
            val intent = Intent(INSTANCE, QRScanActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            ContextCompat.startActivity(INSTANCE!!, intent, null)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivityComponent().inject(this)
        setContentView(R.layout.activity_qr_scan)
        qrScanPresenter.handleOnCreate(qrScanView, qrScanRouter)
    }

    override fun onResume() {
        super.onResume()
        qrScanPresenter.handleOnResume()
    }

    override fun onPause() {
        super.onPause()
        qrScanPresenter.handleOnPause()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return qrScanPresenter.handleOnCreateOptionsMenu(menu)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        qrScanPresenter.handleOnRequestPermissionResult(requestCode, permissions, grantResults)
    }
}