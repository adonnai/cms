package com.android.bpml.reader.model

interface QRScanInteractor {
    fun uploadDetails()
}