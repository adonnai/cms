package com.android.bpml.reader.router

import android.support.v7.app.AppCompatActivity
import javax.inject.Inject

class QRScanRouterImpl @Inject constructor(val activity : AppCompatActivity) : QRScanRouter