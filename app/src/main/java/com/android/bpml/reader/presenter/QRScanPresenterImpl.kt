package com.android.bpml.reader.presenter

import android.content.pm.PackageManager
import android.view.Menu
import com.android.bpml.CMSApp
import com.android.bpml.INSTANCE
import com.android.bpml.R
import com.android.bpml.reader.model.QRScanInteractor
import com.android.bpml.reader.router.QRScanRouter
import com.android.bpml.reader.view.QRScanView
import com.android.bpml.utils.CAMERA_REQUEST_CODE
import com.android.bpml.utils.CmsAppLog
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import javax.inject.Inject

class QRScanPresenterImpl : QRScanPresenter,  ZXingScannerView.ResultHandler {
    @Inject lateinit var interactor : QRScanInteractor

    lateinit var view: QRScanView

    lateinit var router: QRScanRouter
    private val LOGGER : CmsAppLog

    init {
        (INSTANCE as CMSApp).cmsAppComponent.inject(this)
        LOGGER = (INSTANCE as CMSApp).appComponent.getCmsAppLog()
    }

    override fun handleOnCreate(qrScanView: QRScanView, qrScanRouter: QRScanRouter) {
        this.view = qrScanView
        this.router = qrScanRouter
        qrScanView.initViews(this)
    }

    override fun handleOnResume() {
        view.handleOnResume()
    }

    override fun handleOnPause() {
        view.handleOnPause()
    }

    override fun handleResult(rawResult: Result?) {
        LOGGER.e("dhara", rawResult?.text)
        LOGGER.e("dhara", rawResult?.barcodeFormat.toString())
        view.updateView(rawResult?.text)

        // update view to display what is scanned
        view.resumeCameraPreview(this)
    }

    override fun handleOnCreateOptionsMenu(menu: Menu?) : Boolean {
        return view.handleOnCreateOptionsMenu(menu)
    }

    override fun handleOnRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    view.handleOnResume()
                } else {
                    view.showMessage(INSTANCE?.getString(R.string.permission_needed))
                }
                return
            }
        }
    }
}
