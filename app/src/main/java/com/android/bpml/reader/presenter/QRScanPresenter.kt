package com.android.bpml.reader.presenter

import android.view.Menu
import com.android.bpml.reader.router.QRScanRouter
import com.android.bpml.reader.view.QRScanView

interface QRScanPresenter {
    fun handleOnCreate(qrScanView : QRScanView, qrScanRouter : QRScanRouter)

    fun handleOnResume()

    fun handleOnPause()

    fun handleOnCreateOptionsMenu(menu : Menu?) : Boolean

    fun handleOnRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
}