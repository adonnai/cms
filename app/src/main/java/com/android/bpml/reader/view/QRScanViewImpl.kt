package com.android.bpml.reader.view

import android.Manifest
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.android.bpml.R
import com.android.bpml.utils.CAMERA_REQUEST_CODE
import com.android.bpml.utils.extensions.setVisible
import com.android.bpml.utils.showSnackbar
import com.google.zxing.BarcodeFormat
import kotlinx.android.synthetic.main.activity_qr_scan.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import javax.inject.Inject

class QRScanViewImpl @Inject constructor(val activity : AppCompatActivity) : QRScanView {

    lateinit var scannerView : ZXingScannerView
    lateinit var handler : ZXingScannerView.ResultHandler
    var flash : Boolean = false
    var autoFocus : Boolean = false

    override fun initViews(handler: ZXingScannerView.ResultHandler) {
        this.handler = handler

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.CAMERA),
                    CAMERA_REQUEST_CODE
            )
        }

        initViews()
    }

    override fun handleOnResume() {
        scannerView.setResultHandler(handler)
        scannerView.startCamera()
        scannerView.setAutoFocus(true)
    }

    override fun handleOnPause() {
        scannerView.stopCamera()
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun showMessage(message: String?) {
        activity.rootView.showSnackbar(message, Snackbar.LENGTH_SHORT)
    }

    override fun resumeCameraPreview(handler : ZXingScannerView.ResultHandler) {
        scannerView.resumeCameraPreview(handler)
    }

    override fun handleOnCreateOptionsMenu(menu: Menu?): Boolean {
        var menuItem = menu?.add(Menu.NONE,
                R.id.menu_flash,
                0,
                if (flash) R.string.flash_on else R.string.flash_off)
        menuItem?.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER)

        menuItem = menu?.add(Menu.NONE,
                R.id.menu_auto_focus,
                0,
                if (autoFocus) R.string.auto_focus_on else R.string.auto_focus_off)
        menuItem?.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER)

        return true
    }

    override fun updateView(result: String?) {
        activity.txtResult.setVisible(!result.isNullOrBlank())
        result?.let { activity.txtResult.text = result }
    }

    private fun initViews() {
        activity.setSupportActionBar(activity.toolbar)
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        activity.supportActionBar?.setHomeButtonEnabled(false)

        fun setupFormats() : ArrayList<BarcodeFormat> {
            val formats = ArrayList<BarcodeFormat>()
            for (format : BarcodeFormat in ZXingScannerView.ALL_FORMATS) {
                if (format == BarcodeFormat.QR_CODE) {
                    formats.add(format)
                    break
                }
            }
            return formats
        }

        scannerView = ZXingScannerView(activity)
        scannerView.setFormats(setupFormats())
        activity.contentFrame.addView(scannerView)
    }
}