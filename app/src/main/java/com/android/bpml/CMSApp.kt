package com.android.bpml

import android.app.Application
import android.content.Context
import com.android.bpml.dagger2.components.*
import com.android.bpml.dagger2.modules.AppModule
import com.android.bpml.dagger2.modules.CmsAppModule
import com.android.bpml.dagger2.modules.NetModule
import com.android.bpml.network.api.getHost

var INSTANCE : CMSApp? = null

fun getInstance() : CMSApp? {
    return INSTANCE
}

class CMSApp : Application() {
    val appComponent : AppComponent by lazy {
        DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    val cmsAppComponent : CmsAppComponent by lazy {
        DaggerCmsAppComponent.builder()
                .appModule(AppModule(this))
                .cmsAppModule(CmsAppModule())
                .build()
    }

    val netComponent : NetComponent by lazy {
        DaggerNetComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule(getHost()))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        init(this)
    }

    fun init(context : Context) {
        if (INSTANCE == null) {
            INSTANCE = context as CMSApp
        }
    }
}