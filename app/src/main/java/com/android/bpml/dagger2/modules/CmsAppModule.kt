package com.android.bpml.dagger2.modules

import com.android.bpml.login.model.LoginInteractor
import com.android.bpml.login.model.LoginInteractorImpl
import com.android.bpml.reader.model.QRScanInteractor
import com.android.bpml.reader.model.QRScanInteractorImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class CmsAppModule {
    @Provides
    fun provideGson() : Gson {
        return Gson()
    }

    // other interactors and date source here
    @Provides
    fun provideLoginInteractor() : LoginInteractor = LoginInteractorImpl()

    @Provides
    fun provideQRScanInteractor() : QRScanInteractor = QRScanInteractorImpl()
}