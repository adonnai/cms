package com.android.bpml.dagger2.components

import com.android.bpml.dagger2.modules.AppModule
import com.android.bpml.login.presenter.LoginPresenterImpl
import com.android.bpml.dagger2.modules.CmsAppModule
import com.android.bpml.reader.presenter.QRScanPresenterImpl
import dagger.Component

@Component(modules = [CmsAppModule::class, AppModule::class])
interface CmsAppComponent {
    fun inject(loginPresenter : LoginPresenterImpl)

    fun inject(loginPresenter: QRScanPresenterImpl)
}