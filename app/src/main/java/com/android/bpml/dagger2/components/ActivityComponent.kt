package com.android.bpml.dagger2.components

import com.android.bpml.dagger2.injection.PerActivity
import com.android.bpml.dagger2.modules.ActivityModule
import com.android.bpml.login.view.LoginActivity
import com.android.bpml.reader.view.QRScanActivity
import dagger.Component

@PerActivity
@Component(modules = [ActivityModule::class], dependencies = [AppComponent::class, CmsAppComponent::class])
interface ActivityComponent {
    fun inject(loginActivity: LoginActivity)

    fun inject(qrScanActivity: QRScanActivity)
}