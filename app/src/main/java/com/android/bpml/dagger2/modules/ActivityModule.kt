package com.android.bpml.dagger2.modules

import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.android.bpml.dagger2.injection.PerActivity
import com.android.bpml.dagger2.scopes.ActivityScope
import com.android.bpml.login.presenter.LoginPresenter
import com.android.bpml.login.presenter.LoginPresenterImpl
import com.android.bpml.login.router.LoginRouter
import com.android.bpml.login.router.LoginRouterImpl
import com.android.bpml.login.view.LoginView
import com.android.bpml.login.view.LoginViewImpl
import com.android.bpml.reader.presenter.QRScanPresenter
import com.android.bpml.reader.presenter.QRScanPresenterImpl
import com.android.bpml.reader.router.QRScanRouter
import com.android.bpml.reader.router.QRScanRouterImpl
import com.android.bpml.reader.view.QRScanView
import com.android.bpml.reader.view.QRScanViewImpl
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: AppCompatActivity) {
    @Provides
    fun provideActivity(): AppCompatActivity = activity

    @Provides
    @ActivityScope
    fun provideContext(): Context = activity

    @Provides
    @PerActivity
    fun provideLoginPresenter(): LoginPresenter = LoginPresenterImpl()

    @Provides
    @PerActivity
    fun provideLoginView(): LoginView = LoginViewImpl(activity)

    @Provides
    @PerActivity
    fun provideLoginRouter() : LoginRouter = LoginRouterImpl(activity)

    @Provides
    @PerActivity
    fun provideQRScanPresenter() : QRScanPresenter = QRScanPresenterImpl()

    @Provides
    @PerActivity
    fun provideQRScanView() : QRScanView = QRScanViewImpl(activity)

    @Provides
    @PerActivity
    fun provideQRScanRouter() : QRScanRouter = QRScanRouterImpl(activity)
}