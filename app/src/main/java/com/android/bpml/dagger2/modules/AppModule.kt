package com.android.bpml.dagger2.modules

import android.app.Application
import com.android.bpml.utils.CmsAppLog
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class AppModule (private val application : Application){

    @Provides
    fun provideApplication() : Application = application

    @Provides
    fun provideCmsAppLog() : CmsAppLog {
        return CmsAppLog()
    }
}