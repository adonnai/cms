package com.android.bpml.dagger2.components

import android.app.Application
import com.android.bpml.CMSApp
import com.android.bpml.dagger2.modules.AppModule
import com.android.bpml.utils.CmsAppLog
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [AppModule::class])
interface AppComponent {
    fun inject(app: CMSApp)

    fun inject(cmsAppLog : CmsAppLog)

    fun application() :Application

    fun getCmsAppLog() : CmsAppLog
}