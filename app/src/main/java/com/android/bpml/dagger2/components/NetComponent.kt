package com.android.bpml.dagger2.components

import com.android.bpml.dagger2.modules.AppModule
import com.android.bpml.dagger2.modules.NetModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [AppModule:: class, NetModule::class])
interface NetComponent {
    // inject all data sources here
}