package com.android.bpml.utils

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View

fun showSnackBar(view: View, @StringRes message: Int, duration: Int = Snackbar.LENGTH_LONG) {
    view.showSnackbar(view.resources.getString(message), duration)
}

fun View.showSnackbar(message: String?, duration: Int = Snackbar.LENGTH_LONG) {
    message?.let {
        Snackbar.make(this, message, duration).show()
    }
}