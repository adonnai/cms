package com.android.bpml.utils

import android.util.Log
import com.android.bpml.BuildConfig
import com.android.bpml.INSTANCE
import com.android.bpml.CMSApp

class CmsAppLog {
    init {
        (INSTANCE as CMSApp).appComponent.inject(this)
    }

    fun d(tag : String, msg: String?) {
        if (useLog()) {
            msg?.let {
                Log.d(tag, msg)
            }
        }
    }

    fun e(tag: String, msg: String?) {
        if (useLog()) {
            msg?.let {
                Log.e(tag, msg)
            }
        }

    }

    fun writeToFile(tag: String, throwable: Throwable) {
        Log.wtf(tag, throwable)
    }

    private fun useLog() : Boolean {
        return BuildConfig.DEBUG
    }
}