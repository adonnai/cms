package com.android.bpml.common

import com.android.bpml.network.ServiceError

interface ResponseListener {
    fun onSuccess()

    fun onError(error : ServiceError)
}