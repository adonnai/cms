package com.android.bpml.common.view

interface BaseView {
    fun showProgress()

    fun hideProgress()

    fun showMessage(message : String?)
}