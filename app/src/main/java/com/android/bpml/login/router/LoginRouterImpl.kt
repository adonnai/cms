package com.android.bpml.login.router

import android.support.v7.app.AppCompatActivity
import com.android.bpml.reader.view.QRScanActivity
import javax.inject.Inject

class LoginRouterImpl @Inject constructor(val activity : AppCompatActivity): LoginRouter {
    override fun navigateToScan() {
        QRScanActivity.startActivity()
        close()
    }

    override fun close() {
        activity.finish()
    }
}