package com.android.bpml.login.view

import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.android.bpml.INSTANCE
import com.android.bpml.R
import com.android.bpml.utils.showSnackbar
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginViewImpl @Inject constructor(val activity: AppCompatActivity): LoginView {
    lateinit var listener: InteractionListener

    override fun initViews(listener: InteractionListener) {
        this.listener = listener
        activity.btnLogin.setOnClickListener { validateLogin() }
        activity.etEmail.addTextChangedListener(CustomTextWatcher(activity.tilEmail))
        activity.etPassword.addTextChangedListener(CustomTextWatcher(activity.tilPassword))
    }

    override fun showProgress() {
        activity.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        activity.progressBar.visibility = View.GONE
    }

    override fun showMessage(message: String?) {
        if (!message.isNullOrBlank()) {
            activity.rootView.showSnackbar(message.toString(), Snackbar.LENGTH_SHORT)
        }
    }

    override fun getEmail(): String {
        return activity.etEmail.text.toString()
    }

    override fun getPassword(): String {
        return activity.etPassword.text.toString()
    }

    private fun validateLogin() {
        if (activity.etEmail.text?.toString().isNullOrBlank()) {
            activity.tilEmail.isErrorEnabled =  true
            activity.tilEmail.error = INSTANCE?.getString(R.string.field_cannot_be_blank_error)
            return
        }

        if (activity.etPassword.text?.toString().isNullOrBlank()) {
            activity.tilPassword.isErrorEnabled =  true
            activity.tilPassword.error = INSTANCE?.getString(R.string.field_cannot_be_blank_error)
            return
        }

        listener.onLoginClicked()
    }

    class CustomTextWatcher(private var tilInput : TextInputLayout) : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            tilInput.error =
                    if (s?.toString().isNullOrEmpty()) INSTANCE?.getString(R.string.field_cannot_be_blank_error)
                    else null

            tilInput.isErrorEnabled = s?.toString().isNullOrEmpty()
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }
}