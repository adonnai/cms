package com.android.bpml.login.router

interface LoginRouter {
    fun navigateToScan()

    fun close()
}