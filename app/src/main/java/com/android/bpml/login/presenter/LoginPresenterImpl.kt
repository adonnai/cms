package com.android.bpml.login.presenter

import com.android.bpml.CMSApp
import com.android.bpml.INSTANCE
import com.android.bpml.common.ResponseListener
import com.android.bpml.login.model.LoginInteractor
import com.android.bpml.login.router.LoginRouter
import com.android.bpml.login.view.InteractionListener
import com.android.bpml.login.view.LoginView
import com.android.bpml.network.ServiceError
import javax.inject.Inject

class LoginPresenterImpl : LoginPresenter, InteractionListener, ResponseListener {
    @Inject
    lateinit var interactor: LoginInteractor
    lateinit var view : LoginView
    lateinit var router : LoginRouter

    init {
        (INSTANCE as CMSApp).cmsAppComponent.inject(this)
    }

    override fun onLoginClicked() {
        //view.showProgress()
        //interactor.login(view.getEmail(), view.getPassword())
        router.navigateToScan()
    }

    override fun handleOnCreate(view: LoginView, router: LoginRouter) {
        this.view = view
        this.router = router
        view.initViews(this)
    }

    override fun onSuccess() {
        view.hideProgress()
        //router.navigateToHome()
    }

    override fun onError(error: ServiceError) {
        view.hideProgress()
        view.showMessage(error.errorMessage)
    }
}