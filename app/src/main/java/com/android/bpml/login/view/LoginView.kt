package com.android.bpml.login.view

import com.android.bpml.common.view.BaseView

interface LoginView : BaseView {
    fun initViews(listener: InteractionListener)

    fun getEmail() : String

    fun getPassword() : String
}