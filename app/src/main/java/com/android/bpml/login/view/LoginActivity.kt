package com.android.bpml.login.view

import android.os.Bundle
import com.android.bpml.BaseActivity
import com.android.bpml.R
import com.android.bpml.login.presenter.LoginPresenter
import com.android.bpml.login.router.LoginRouter
import javax.inject.Inject

class LoginActivity : BaseActivity() {
    @Inject
    lateinit var loginPresenter : LoginPresenter

    @Inject
    lateinit var loginView : LoginView

    @Inject
    lateinit var loginRouter : LoginRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        getActivityComponent().inject(this)
        loginPresenter.handleOnCreate(view = loginView, router = loginRouter)
    }
}