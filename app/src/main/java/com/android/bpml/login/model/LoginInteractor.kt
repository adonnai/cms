package com.android.bpml.login.model

interface LoginInteractor {
    fun login(email: String, password: String)
}