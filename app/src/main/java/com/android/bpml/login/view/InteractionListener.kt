package com.android.bpml.login.view

interface InteractionListener {
    fun onLoginClicked()
}