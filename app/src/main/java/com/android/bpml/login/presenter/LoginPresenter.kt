package com.android.bpml.login.presenter

import com.android.bpml.login.router.LoginRouter
import com.android.bpml.login.view.LoginView

interface LoginPresenter {
    fun handleOnCreate(view: LoginView, router: LoginRouter)
}