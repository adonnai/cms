package com.android.bpml.network

data class ServiceError @JvmOverloads constructor(var errorMessage: String? = "",
                                                  var errorCode: String? = "")